# GitHub Release Watcher

We are interested in new Knative versions and new releases of some other tools. This app provides
a simple serverless app that checks given repos for releases published in the past day.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ypRgs-wCflQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Usage

This app can be used in 3 ways:

- from the gitlab chat operator `/gitlab knative-examples/github-release-watcher run check-repos fog/fog-google`
- from cli `curl -s --header "Content-Type: application/json" --data '{"repos":"aws/aws-sdk-go"}' http://functions-github-release-watcher.github-release-watcher-16373815-production.gc2.nagyv.com`
- as a scheduled CI job that posts its output to a dedicated Slack channel

## Setup

If you would like to run the GitHub Release Watcher for yourself you can do it in 2 different ways

### Running it on your own infrastructure

This setup assumes, you have a running Kubernetes cluster.

1. Fork this repo
1. Add your cluster as a GitLab managed cluster under your repo or group
1. Install Knative as a GitLab managed app
1. Start a deployment pipeline
1. Check your functions deployment url under `Operations / Serverless`, and change your URL at the top of `.gitlab-ci.yml`
1. (optional) Set up a scheduled job to run the release watcher at regular intervals. The script expects 2 environment variables, `REPOS` and `SLACK_INCOMING_WEBHOOK` variables.
   - `REPOS` is a comma separated list of GitHub repos to watch (e.g. `knative/serving, knative/eventing, fog/fog-google`)
   - `SLACK_INCOMING_WEBHOOK` is the webhook URL to post to a specific Slack channel. It's really [easy to set up](https://api.slack.com/messaging/webhooks).
1. (optional) Set up [Slack slash command](https://docs.gitlab.com/ee/user/project/integrations/slack_slash_commands.html) to run the release watcher on a per request basis.

### Running it the easy way

If you don't need to be mission critical with this, you can use our deployment for your own needs. We do not guarantee
that the cluster running this code will be always available and reachable!

1. Create a new GitLab repo (or fork the current one)
1. (optional) Set up a scheduled job to run the release watcher at regular intervals. The script expects 2 environment variables, `REPOS` and `SLACK_INCOMING_WEBHOOK` variables.
   - `REPOS` is a comma separated list of GitHub repos to watch (e.g. `knative/serving, knative/eventing, fog/fog-google`)
   - `SLACK_INCOMING_WEBHOOK` is the webhook URL to post to a specific Slack channel. It's really [easy to set up](https://api.slack.com/messaging/webhooks).
1. (optional) Set up [Slack slash command](https://docs.gitlab.com/ee/user/project/integrations/slack_slash_commands.html) to run the release watcher on a per request basis.
