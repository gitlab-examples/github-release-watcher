"use strict"
const axios = require('axios').default

class ResponseError extends Error {
  constructor(axiosError, repo) {
    super(axiosError.response.statusText)
    this.status = axiosError.response.status
    this.repo = repo
  }
}

function yesterday() {
  var date = new Date();
  date.setDate(date.getDate() - 1);
  return date.getTime()
}

async function getRepoReleases(repo) {
  repo = repo.trim()
  let response = null
  try {
    response = await axios.get(`https://api.github.com/repos/${repo}/releases`)
  } catch(e) {
    throw new ResponseError(e, repo)
  }

  if(response.data.length === 0) {
    return null
  }

  const {name, body, published_at, html_url} = response.data[0]
  if(Date.parse(published_at) > yesterday()) {
    return {repo, name, body, published_at, html_url}
  }
  return null
}

module.exports = getRepoReleases
module.exports.ResponseError = ResponseError