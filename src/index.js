"use strict"
const { Message } = require('@projectriff/message')
const getRepoRelease = require('./releases')

function createMessage(releases) {
  return releases.map(({repo, name, published_at, html_url}) => {
    return `There is a new release in ${repo}: ${name}. It was published on ${published_at}. For more info visit ${html_url}`
  }).join('\n\n')
}

function createErrorMessage({status, message, repo}) {
  return `The repo "${repo}" says: "${message}" (code: ${status})`
}

async function getNewReleaseData(repos) {
  const promises = repos.map(getRepoRelease)
  let releases = []
  try {
    releases = await Promise.all(promises)
  } catch (e) {
    throw e
  }
  return releases.filter(release => release != null)
}

async function checkAndNotifyAboutReleases ({payload}) {
  const repos = payload.repos.split(',')
  let message = ''
  try {
    const releases = await getNewReleaseData(repos)
    message = createMessage(releases)
  } catch (e) {
    message = createErrorMessage(e)
  }
  

  return Message.builder()
    .payload(message)
    .build();
}

module.exports = checkAndNotifyAboutReleases
module.exports.getNewReleaseData = getNewReleaseData
module.exports.$defaultContentType = "application/json";
module.exports.$argumentType = 'message'

Message.install();