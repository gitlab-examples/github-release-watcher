const moxios = require('moxios')
const should = require('should')
const getRepoReleases = require('../releases')

describe('Get repo releases', function() {
  beforeEach(function () {
    // import and pass your custom axios instance to this method
    moxios.install()
  })

  afterEach(function () {
    // import and pass your custom axios instance to this method
    moxios.uninstall()
  })
  describe('If no recent release exists', function() {
    it('it returns null', async function() {
      moxios.stubRequest(/.*team\/old-release.*/, {
        status: 200,
        response: [{
          name: 'outdated',
          body: 'outdated',
          published_at: '2019-12-24T10:45:22Z',
          html_url: ''
        }]
      })

      const response = await getRepoReleases('team/old-release')
      return should(response).be.null()
    });
  });
  describe('if new release exists', function() {
    it('returns important release data', async function () {
      const now = new Date()
      moxios.stubRequest(/.*team\/new-release.*/, {
        status: 200,
        response: [{
          name: 'outdated',
          body: 'outdated',
          published_at: now.toISOString(),
          html_url: ''
        }]
      })

      const response = await getRepoReleases('team/new-release')
      should(response).be.an.Object()
      response.should.only.have.keys('repo', 'name', 'body', 'published_at', 'html_url')
    })
  })
  describe('if api returns an error', function() {
    it('throws a response error', async function() {
      getRepoReleases('nonexistent').should.be.rejectedWith({message: "Not found", status: 404, repo: "nonexistent"})
    })
  })
});
