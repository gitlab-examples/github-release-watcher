const proxyquire = require('proxyquire')
const should = require('should')

const main = proxyquire('../index', {
  './releases': async function(repo) {
    if(repo.startsWith('hasnew/')) {
      return {
        repo, 
        name: 'New release',
        published_at: 'now',
        html_url: 'here'
      }
    } else {
      return null
    }
  }
})

describe('Main', () => {
  it('expects messages', () => {
    should(main.$argumentType).be.equal('message')
  })
  describe('getNewReleaseData', () => {
    it('returns a list by default', async () => {
      const newData = await main.getNewReleaseData([])
      should(newData).be.eql([])
    })
    it('returns a list of new releases', async () => {
      const newData = await main.getNewReleaseData(['hasnew/first', 'old/old', 'hasnew/second'])
      newData.should.have.length(2)
    })
  })
})